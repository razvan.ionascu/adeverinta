<?php
/**
 * Created by Razvan <razvan@razvanionascu.com>.
 */

return [
    'twig' => [
        'cache_dir'       => 'data/cache/twig',
        'assets_url'      => '/public/assets/',
        // 'timezone' => 'default timezone identifier; e.g. America/Chicago',
    ],
];