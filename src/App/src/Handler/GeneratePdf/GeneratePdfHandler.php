<?php

declare(strict_types=1);

namespace App\Handler\GeneratePdf;

use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Laminas\Diactoros\Uri;
use Mezzio\Router;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class GeneratePdfHandler implements RequestHandlerInterface
{

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    )
    {
        $this->router   = $router;
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {

        $data = $request->getParsedBody();
        if (empty($data)) {
//            echo '<pre>';
//            var_export($request);die;
            return new RedirectResponse(new Uri('/'));
        }
//        return new HtmlResponse($this->template->render('app::adeverinta', $data));
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($this->template->render('app::adeverinta', $data));
        $mpdf->Output();


    }
}
